#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#export PATH=$PATH:$HOME/bin:$HOME/dwmscripts:/usr/local/bin

#Colorise some commands
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias cat='ccat'
alias df='dfc'
alias lspci='grc lspci'
alias ifconfig='grc ifconfig'
alias ps='grc ps aux'
alias lsmod='grc lsmod'
alias ping='grc ping'
alias mount='grc mount'
alias lsblk='grc lsblk'
alias du='grc du -h'

#alias ls='ls --color=auto'
alias ls='grc ls'
alias ll='grc ls -lh'
alias la='grc ls -lah'

#Colorful man pages
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

#PS1='[\u@\h \W]\$ '
export PS1="\[\e[1;33m\][\[\e[m\]\[\e[1;33m\]\u\[\e[m\]\[\e[1;33m\]]\[\e[m\]\[\e[1;34m\][\[\e[m\]\[\e[1;34m\]\w\[\e[m\]\[\e[1;34m\]]\[\e[m\]\[\e[1;31m\]-\[\e[m\]\[\e[1;31m\]>\[\e[m\] "

#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

# Aliases for software managment
alias pacup='sudo pacman -Syu'
alias yayup='yay -Syu'

#Clean orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

#Clean AUR orphans
alias cleanyay='yay -Yc'

#Clean cache
alias cache='rm -rf ~/.cache/*'

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

#git commands
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
